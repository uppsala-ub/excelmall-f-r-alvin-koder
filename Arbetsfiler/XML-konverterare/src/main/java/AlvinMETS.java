
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Writer;
import java.util.*;

/**
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AlvinMETS {

    public List<Filförteckning> metsAlvinSet;

    public AlvinMETS() {}

    public AlvinMETS(List<Filförteckning> filförteckningar) {
        this.metsAlvinSet = filförteckningar;
    }

    public static void marshalXml(Writer out, List<Filförteckning> filförteckning) {
        JAXB.marshal(new AlvinMETS(filförteckning),out);
    }

}
