import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 */
public class AlvinPaketerare {


    public static void main(String... args) throws IOException {
        if(args.length>0) {
            processWithArgs(args);
        } else {
            processWithConfigFile();
        }
    }

    public static void processWithArgs(String... args) throws IOException {
        final StringWriter metsString = new StringWriter();
        AlvinMETS.marshalXml(metsString, Filförteckning.load(args[0]));
        System.out.println(metsString);
    }

    public static void processWithConfigFile() throws IOException {
        Config config;
        if (Files.exists(Paths.get("config.properties")))
            config = Config.load("config.properties");
        else
            config = Config.load(AlvinPaketerare.class.getResourceAsStream("config.properties"));

        Writer metsFile = Files.newBufferedWriter(Paths.get(config.metsPath));
        AlvinMETS.marshalXml(metsFile, Filförteckning.load(config.xslxPath));
    }

}
