import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wolny381.
 */
public class Filförteckning {
    public String Ordningstal;
    public String Etikett;
    public String Etikettkod;
    public String Bildsökväg;

    public static List<Filförteckning> load(String xslxPath) throws IOException {

        final XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(Paths.get(xslxPath)));

        final XSSFSheet sheetAt = workbook.getSheetAt(0);
        final List<Filförteckning> collect = new ArrayList<>();
        sheetAt.iterator().forEachRemaining(p -> {
            Filförteckning record = new Filförteckning();
            record.Ordningstal = extractValue(p.getCell(7, Row.CREATE_NULL_AS_BLANK));
            record.Etikett = extractValue(p.getCell(8, Row.CREATE_NULL_AS_BLANK));
            record.Etikettkod = extractValue(p.getCell(9, Row.CREATE_NULL_AS_BLANK));
            record.Bildsökväg = extractValue(p.getCell(10, Row.CREATE_NULL_AS_BLANK));

            collect.add(record);
        });

        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return collect.stream()
                .skip(1)
                .filter(f -> !f.Etikett.trim().isEmpty())
                .sorted((f1, f2) -> Integer.valueOf(f1.Ordningstal) > Integer.valueOf(f2.Ordningstal) ? 1 : -1)
                .collect(Collectors.toList());
    }

    public static String extractValue(Cell cell) {
        int cellType = cell.getCellType();
        if(cell.getCellType() == Cell.CELL_TYPE_FORMULA)
            cellType = cell.getCachedFormulaResultType();
        switch (cellType) {
            case Cell.CELL_TYPE_NUMERIC:
                return String.format("%.0f", cell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            default:
                return cell.toString();
        }
    }
}
