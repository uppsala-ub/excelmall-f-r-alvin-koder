import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 */
public class Config {


    public String xslxPath = "Y53_klar.xlsx";
    public String metsPath = "metx.xml";


    public static Config load(String configPath) throws IOException {
        Properties prop = new Properties();
        Config config = new Config();
        try(InputStream input = new FileInputStream(configPath)) {
            config = load(input);
        }
        return config;
    }

    public static Config load(InputStream input) throws IOException {
        Properties prop = new Properties();
        Config config = new Config();

        prop.load(input);
        config.metsPath = prop.getProperty("xslxOut",config.metsPath);
        config.xslxPath = prop.getProperty("xslxPath",config.xslxPath);

        return config;
    }
}
