import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by wolny381.
 */
public class Excel2MetsTest {


    static Config config;

    @BeforeClass
    public static void loadConfig() throws IOException {
        config = Config.load(Excel2MetsTest.class.getResourceAsStream("config.properties"));
    }

    @Test
    public void readExcelFile() throws IOException {
        Filförteckning.load(config.xslxPath).forEach((v) -> System.out.println(v.Etikett));
    }

    @Test
    public void saveMetsFile() throws IOException {
        AlvinPaketerare.main("Y53_klar.xlsx");
    }
}
